﻿using UnityEngine;
using System.Collections;

public class ContinueButtonScript : MonoBehaviour {
	public Sprite continueButtonDown;
	public Sprite continueButtonUp;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = continueButtonDown;
	}
	
	void OnMouseUp(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = continueButtonUp;
		Application.LoadLevel (Application.loadedLevel);
		
	}
}
