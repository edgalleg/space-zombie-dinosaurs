﻿using UnityEngine;
using System.Collections;

public class GridBubbleManager : MonoBehaviour {
	public System.Collections.Generic.HashSet<GameObject> gridBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	private GameObject grid_bubble;

	// Use this for initialization
	void Start () {

		//Add each gridBubble
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("grid")){
				gridBubbleList.Add(go);

			}
		}

		//Remove each gridbubble being occupied by StaticBubbles
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("static")){
				grid_bubble = placement(go.transform.position.x,go.transform.position.y);
				gridBubbleList.Remove(grid_bubble);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//This function returns the gridbubble closest to the object given its x and y position.
	GameObject placement(float x,float y){
		GameObject chosenBubble = gameObject;
		float minimum = 500.0f;
		foreach (GameObject g in gridBubbleList) {
			float value = Mathf.Sqrt(Mathf.Pow ((x - g.transform.position.x),2) + Mathf.Pow ((y - g.transform.position.y),2));
			
			if(value < minimum){
				minimum = value;
				chosenBubble = g;
			}
		}

		return chosenBubble;
	}
}
