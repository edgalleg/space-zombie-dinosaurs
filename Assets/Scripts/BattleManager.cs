﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour {
	public float enemy_health;
	public float baseEnemyHealth;
	public int baseHealth;
	public int attackDamage;
	public int health;
	public GameObject monster;
	private Sprite monsterSprite;
	public Sprite redmonster;
	public Sprite greenmonster;
	public Sprite bluemonster;
	public Sprite yellowmonster;
	public Sprite purplemonster;
	public Sprite yellowbubble;
	public Sprite purplebubble;
	public Sprite medRedMonster;
	public Sprite medGreenMonster;
	public Sprite medBlueMonster;
	public Sprite medYellowMonster;
	public Sprite medPurpleMonster;
	public Sprite zomRedMonster;
	public Sprite zomBlueMonster;
	public Sprite zomGreenMonster;
	public Sprite zomYellowMonster;
	public Sprite zomPurpleMonster;
	public Sprite rexRedMonster;
	public Sprite rexBlueMonster;
	public Sprite rexGreenMonster;
	public Sprite rexYellowMonster;
	public Sprite rexPurpleMonster;
	public Sprite rexMedRedMonster;
	public Sprite rexMedBlueMonster;
	public Sprite rexMedGreenMonster;
	public Sprite rexMedYellowMonster;
	public Sprite rexMedPurpleMonster;
	public Sprite rexZomRedMonster;
	public Sprite rexZomBlueMonster;
	public Sprite rexZomGreenMonster;
	public Sprite rexZomYellowMonster;
	public Sprite rexZomPurpleMonster;
	public Sprite redbubble;
	public Sprite greenbubble;
	public Sprite bluebubble;
	public GameObject cannon;
	public int tbReady;
	public int turns;
	private int originalTurns;
	public bool stopGame;
	public System.Collections.Generic.List<Sprite> spriteList = new System.Collections.Generic.List<Sprite>();
	Vector3 monsterPosition;
	public string gameMode;
	public int round;
	public GameObject resultCanvas;
	public GameObject continueButton;
	public GameObject levelSelectButton;
	public GameObject RestartButton;
	public Text resultsText;
	public bool popupOn;
	public GameObject Popup1;
	public GameObject EnemyHealthBar;
	public GameObject topWall;
	public GameObject explosion;
	public AudioSource explosionSound;
	public ParticleSystem bloodParticles;
	public AudioSource dinoNoise;
	private int monsterSelection;
	public GameObject wall_top;


	// Use this for initialization
	void Start () {
		cannon.SetActive (true);
		explosionSound = GetComponent<AudioSource> ();
		monster.SetActive (true);
		gameMode = PlayerPrefs.GetString ("mode");
		monsterSelection = PlayerPrefs.GetInt ("survival");
		if (PlayerPrefs.GetString ("mode") == "survival") {
			attackDamage = UnityEngine.Random.Range (2, 6);
			turns = UnityEngine.Random.Range (2, 5);
			spriteList.Add (redmonster);
			spriteList.Add (bluemonster);
			spriteList.Add (greenmonster);
			spriteList.Add (yellowmonster);
			spriteList.Add (purplemonster);
			spriteList.Add (rexRedMonster);
			spriteList.Add(rexGreenMonster);
			spriteList.Add (rexBlueMonster);
			spriteList.Add (rexYellowMonster);
			spriteList.Add (rexPurpleMonster);
			spriteList.Add (medRedMonster);
			spriteList.Add(medBlueMonster);
			spriteList.Add (medGreenMonster);
			spriteList.Add(medYellowMonster);
			spriteList.Add (medPurpleMonster);
			spriteList.Add (rexMedRedMonster);
			spriteList.Add(rexMedBlueMonster);
			spriteList.Add (rexMedGreenMonster);
			spriteList.Add(rexMedYellowMonster);
			spriteList.Add (rexMedPurpleMonster);
			spriteList.Add (zomRedMonster);
			spriteList.Add (zomBlueMonster);
			spriteList.Add (zomGreenMonster);
			spriteList.Add (zomYellowMonster);
			spriteList.Add (zomPurpleMonster);
			spriteList.Add (rexZomRedMonster);
			spriteList.Add (rexZomBlueMonster);
			spriteList.Add (rexZomGreenMonster);
			spriteList.Add (rexZomYellowMonster);
			spriteList.Add (rexZomPurpleMonster);
			if(monsterSelection < 5){
				int babyChoice = UnityEngine.Random.Range (0,2);
				if(babyChoice == 1){
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection + 5];
				}
				else{
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection];

				}
			}
			else if(monsterSelection < 10){
				int babyChoice = UnityEngine.Random.Range (0,2);
				if(babyChoice == 1){
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection + 10];
				}
				else{
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection + 5];
					
				}
			}
			else{
				int babyChoice = UnityEngine.Random.Range (0,2);
				if(babyChoice == 1){
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection + 15];
				}
				else{
					monster.GetComponent<SpriteRenderer> ().sprite= spriteList [monsterSelection + 10];
					
				}

			}
			enemy_health = UnityEngine.Random.Range (40 + monsterSelection * 10,50 + monsterSelection * 10);
			baseEnemyHealth = enemy_health;
		}

		originalTurns = turns;
		monsterPosition = monster.transform.position;
		stopGame = false;
		tbReady = 0;
		/*spriteList.Add (redmonster);
		spriteList.Add (bluemonster);
		spriteList.Add (greenmonster);
		spriteList.Add (yellowmonster);
		spriteList.Add (purplemonster); FOR OTHER GAME MODE*/
		//monster.GetComponent<SpriteRenderer> ().sprite= spriteList [UnityEngine.Random.Range (0, 5)];
		monsterSprite = monster.GetComponent<SpriteRenderer> ().sprite;
	
		//health = 25;
	
		//enemy_health = UnityEngine.Random.Range (30,46);
        
	}
    	
	// Update is called once per frame
	void LateUpdate () {

        if (stopGame == false)
        {
            if (enemy_health <= 0)
            {
                resultsText.text = "YOU WIN!\n" + "CONTINUE TO LEVEL " + (monsterSelection + 2);
                Instantiate(explosion, monster.transform.position, monster.transform.rotation);
                explosionSound.Play();
                monster.SetActive(false);
                stopGame = true;
                StartCoroutine(GameOver("winner"));

            }
            
        }
	}

	public void enemyAttack(){
			cannon.GetComponent<CannonScript> ().canShoot = true;
	}

	public string determineWeakness(){
		if (monsterSprite == redmonster || monsterSprite.name.Contains("red")) {
			return "Blue";
		} else if (monsterSprite == bluemonster || monsterSprite.name.Contains("blue")) {
			return "Green";
		} else if (monsterSprite == greenmonster || monsterSprite.name.Contains("green")) {
			return "Red";
		} else if (monsterSprite == yellowmonster || monsterSprite.name.Contains("yellow")) {
			return "Purple";
		} else if (monsterSprite == purplemonster || monsterSprite.name.Contains("purple")) {
			return "Yellow";
		}else {
			return "Null";
		}

	}

	public void RestartGame(){
		StartCoroutine (GameOver ("level"));
	}


	IEnumerator GameOver(string result){
		cannon.GetComponent<CannonScript> ().canShoot = false;
		yield return new WaitForSeconds (0);
		resultCanvas.SetActive (true);
		if (result == "winner") {
			if (gameMode == "story") {
				continueButton.gameObject.SetActive(true);
				RestartButton.gameObject.SetActive(false);
				levelSelectButton.gameObject.SetActive(false);
			} else {
				if(monsterSelection == 14){
					resultsText.text = "YOU BEAT THE GAME!!";
					continueButton.gameObject.SetActive(false);
					RestartButton.gameObject.SetActive(false);
					levelSelectButton.gameObject.SetActive(true);
				}else{

					PlayerPrefs.SetInt ("round", round + 1);
					PlayerPrefs.SetInt ("survival",monsterSelection + 1);
					continueButton.gameObject.SetActive(true);
					RestartButton.gameObject.SetActive(false);
					levelSelectButton.gameObject.SetActive(true);
				}

			}
		} else {
			Instantiate(explosion,cannon.transform.position,cannon.transform.rotation);
			explosionSound.Play ();
			cannon.SetActive(false);
			resultsText.text = "You Lose!";
			if (gameMode == "story"){
				continueButton.gameObject.SetActive(false);
				RestartButton.gameObject.SetActive(true);
				levelSelectButton.gameObject.SetActive(true);
			}
			else{
				PlayerPrefs.SetInt("round", 1);
				continueButton.gameObject.SetActive(false);
				RestartButton.gameObject.SetActive(true);
				levelSelectButton.gameObject.SetActive(true);
			}
		}
	}
	public void determineDamage(GameObject go){
		if(monsterSprite.Equals(bluemonster) || monsterSprite.name.Contains("blue")){
			if (go.GetComponent<SpriteRenderer> ().sprite ==greenbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else if(go.GetComponent<SpriteRenderer>().sprite == redbubble){
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,0.5f);
				enemy_health -= 0.5f;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(greenmonster) || monsterSprite.name.Contains("green")){
			if (go.GetComponent<SpriteRenderer> ().sprite ==redbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else if(go.GetComponent<SpriteRenderer>().sprite == bluebubble){
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,0.5f);
				enemy_health -= 0.5f;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(redmonster) || monsterSprite.name.Contains("red")){
			if (go.GetComponent<SpriteRenderer> ().sprite ==bluebubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else if(go.GetComponent<SpriteRenderer>().sprite == greenbubble){
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,0.5f);
				enemy_health -= 0.5f;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(yellowmonster) || monsterSprite.name.Contains("yellow")){
			if (go.GetComponent<SpriteRenderer> ().sprite ==purplebubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else if(go.GetComponent<SpriteRenderer>().sprite == yellowbubble){
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,0.5f);
				enemy_health -= 0.5f;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(purplemonster) || monsterSprite.name.Contains("purple")){
			if (go.GetComponent<SpriteRenderer> ().sprite ==yellowbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else if(go.GetComponent<SpriteRenderer>().sprite == purplebubble){
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,0.5f);
				enemy_health -= 0.5f;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		bloodParticles.Play ();
		dinoNoise.Play ();
	}
}
