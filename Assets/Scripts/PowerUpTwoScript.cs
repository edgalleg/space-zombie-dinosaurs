﻿using UnityEngine;
using System.Collections;

public class PowerUpTwoScript : MonoBehaviour {
	public Sprite power2;
	public GameObject meter;
	public GameObject container;
	public System.Collections.Generic.List<string> ballList = new System.Collections.Generic.List<string>();
	public GameObject monster;
	
	// Use this for initialization
	void Start () {
		ballList.Add ("bubble_small");
		ballList.Add ("bubble_blue");
		ballList.Add ("bubble_purple");
		ballList.Add ("bubble_red");
		ballList.Add ("bubble_yellow");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnMouseDown(){
		if (gameObject.GetComponent<SpriteRenderer> ().sprite == power2) {
			Debug.Log ("POWER 2");
			meter.GetComponent<MeterScript>().powerTwo();
			activatePower();
		} else {
			Debug.Log ("NO POWER");
		}
	}
	
	void activatePower(){
		int determinant = 0;
		string spriteName = monster.GetComponent<SpriteRenderer> ().sprite.name;
		if (spriteName.Contains ("purple")) {
			determinant = 4;
		} else if (spriteName.Contains ("red")) {
			determinant = 1;
		}
		else if (spriteName.Contains ("green")) {
			determinant = 3;
		}
		else if (spriteName.Contains ("blue")) {
			determinant = 0;
		}
		else if (spriteName.Contains ("yellow")) {
			determinant = 2;
		}
		for (int i = 0; i < 25; i++) {
			container.GetComponent<ContainerScript> ().add (ballList [determinant]);
		}
		int ABM = container.GetComponent<ContainerScript> ().ABMAmount;
		int TBA = container.GetComponent<ContainerScript> ().TBAmount;
		container.GetComponent<ContainerScript> ().ABMAmount = ABM + 25;
		container.GetComponent<ContainerScript> ().TBAmount = TBA + 25;

	
	}

}
