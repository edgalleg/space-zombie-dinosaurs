﻿using UnityEngine;
using System.Collections;

public class MeterScript : MonoBehaviour {
	private int meters;
	public GameObject fill;
	public System.Collections.Generic.List<float> yValues = new System.Collections.Generic.List<float>();
	public System.Collections.Generic.List<GameObject> meterList = new System.Collections.Generic.List<GameObject>();
	public GameObject nopower1;
	public GameObject nopower2;
	public Sprite power1;
	public Sprite power2;
	public Sprite nopower;
	public GameObject cannon;

	private float xVal = 3.04f;
	// Use this for initialization
	void Start () {
		meters = 0;
		for (int i = 0; i < 60; i++) {
			yValues.Add(0.55f + (i * 0.059f));
		}
	}
	
	public void add(){
		if(meters != 60){
			meterList.Add (Instantiate(fill, new Vector3(xVal,yValues[meters],0.0f),fill.transform.rotation) as GameObject);
			meters += 1;
			if(meters > 25){
				nopower1.GetComponent<SpriteRenderer>().sprite = power1;

			}
			if(meters == 60){
				nopower2.GetComponent<SpriteRenderer>().sprite = power2;
			}
		}

	}

	public void powerOne(){
		meters -= 25;
		for (int i = 0; i < 25; i++) {
			Destroy (meterList[meterList.Count - 1]);
			meterList.RemoveAt(meterList.Count - 1);
		}
		if (meters < 25) {
			nopower1.GetComponent<SpriteRenderer>().sprite = nopower;
		}
		nopower2.GetComponent<SpriteRenderer>().sprite = nopower;

	}

	public void powerTwo(){
		int counter = meterList.Count;
		meters -= 60;
		for (int i = 0; i < counter; i++) {
			Destroy (meterList[meterList.Count - 1]);
			meterList.RemoveAt(meterList.Count - 1);
		}
		nopower2.GetComponent<SpriteRenderer>().sprite = nopower;
		nopower1.GetComponent<SpriteRenderer>().sprite = nopower;
	}
}
