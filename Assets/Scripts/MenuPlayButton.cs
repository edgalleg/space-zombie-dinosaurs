﻿using UnityEngine;
using System.Collections;

public class MenuPlayButton : MonoBehaviour {
	public Sprite playButtonDown;
	public Sprite playButtonUp;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = playButtonDown;
	}

	void OnMouseUp(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = playButtonUp;
		PlayerPrefs.SetString ("mode", "survival");
		PlayerPrefs.SetInt ("survival", 0);
		Application.LoadLevel ("ExampleScene");

	}


}
