﻿using UnityEngine;
using System.Collections;

public class ContainerScript : MonoBehaviour {
	private float xVal = -3.0f;
	public System.Collections.Generic.List<float> yValues = new System.Collections.Generic.List<float>();
	public System.Collections.Generic.List<Sprite> conSprites = new System.Collections.Generic.List<Sprite>();
	public System.Collections.Generic.List<GameObject> conObjects = new System.Collections.Generic.List<GameObject>();
	private float bottomYVal = -0.48f;
	public GameObject containerBall;
	private int containerSize;
	public Sprite con_green;
	public Sprite con_red;
	public Sprite con_blue;
	public Sprite con_yellow;
	public Sprite con_purple;
	public Sprite con_unbreakable;
	public Sprite green;
	public Sprite red;
	public Sprite blue;
	public Sprite yellow;
	public Sprite purple;
	public Sprite unbreakable;
	public GameObject anime2Bubble;
	public GameObject containerLid;
	private GameObject cBall;
	private GameObject aBall;
	public bool timeToAnimate;
	public int TBAmount;
	public int ABMAmount;
	private int conSize;
	private int conSize2;
	private int MTLAmount;
	public GameObject explosion;
	public AudioSource explosionSound;
	private bool canExplode;
	// Use this for initialization
	void Start () {
		canExplode = true;
		MTLAmount = 0;
		ABMAmount = 0;
		TBAmount = 0;
		timeToAnimate = false;
		containerSize = 0;
		for (int i = 0; i < 10; i++) {
			yValues.Add(bottomYVal + (i * 0.37f));
		}
	}


	// Update is called once per frame
	void Update () {
		if (ABMAmount == TBAmount && (ABMAmount != 0)) {
			if (conSize > 9) {
				if(canExplode){
					Instantiate(explosion,containerLid.transform.position,containerLid.transform.rotation);
					explosionSound.Play ();
					canExplode = false;
				}
				for (int i = conSize2 - 1; i > -1; i--) {
					float step = 20 * Time.deltaTime;
					conObjects [i].transform.position = Vector3.MoveTowards (conObjects [i].transform.position, containerLid.transform.position, step);
					
				}
			}
		}
		for (int i = conSize2 - 1; i > -1; i--) {
			if (conObjects [i].transform.position == containerLid.transform.position) {
				anime2Bubble.GetComponent<SpriteRenderer> ().sprite = conSprites [i];
				aBall = Instantiate (anime2Bubble, conObjects [i].transform.position, conObjects [i].transform.rotation) as GameObject;
				aBall.gameObject.name = "animeBubble2Spawn";
				MTLAmount++;
				Destroy(conObjects[i]);
				conObjects.RemoveAt(i);
			}
		}
		conSize2 = conObjects.Count;

		if (ABMAmount == MTLAmount && (ABMAmount != 0)) {
			ABMAmount = 0;
			TBAmount = 0;
			MTLAmount = 0;
			conSize = 0;
			conSize2 = 0;
			containerSize = 0;
			canExplode = true;

		}
	
	}

	public void add(string g){
		spriteColor (g);
		if (containerSize == 10) {
			conObjects.Add (Instantiate (containerBall, new Vector3 (xVal, yValues [9], 0.0f), containerBall.transform.rotation) as GameObject);
		} else {
			containerSize++;
			conObjects.Add (Instantiate (containerBall, new Vector3 (xVal, yValues [containerSize-1], 0.0f), containerBall.transform.rotation) as GameObject);

		}
		conSize++;
		conSize2++;
	}

	void spriteColor(string g){
		switch (g) {
		case "bubble_small":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_green;
			conSprites.Add(green);
			break;
		case "red_bubble_small":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_red;
			conSprites.Add(red);
			break;
		case "bubble_blue":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_blue;
			conSprites.Add(blue);
			break;
		case "bubble_yellow":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_yellow;
			conSprites.Add (yellow);
			break;
		case "bubble_purple":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_purple;
			conSprites.Add(purple);
			break;

		case "unbreakable_ball":
			containerBall.gameObject.GetComponent<SpriteRenderer>().sprite = con_unbreakable;
			conSprites.Add(unbreakable);
			break;

		}

	}
}
