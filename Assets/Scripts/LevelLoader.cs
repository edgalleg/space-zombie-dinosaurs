﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {
	public string level;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadLevel(){
		Application.LoadLevel (level);
	}

	public void PickLevel(string levelName){
		level = "Level" + levelName;
	}
}
