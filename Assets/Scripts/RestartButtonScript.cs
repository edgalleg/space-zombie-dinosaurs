﻿using UnityEngine;
using System.Collections;

public class RestartButtonScript : MonoBehaviour {
	public Sprite restartButtonDown;
	public Sprite restartButtonUp;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = restartButtonDown;
	}
	
	void OnMouseUp(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = restartButtonUp;
		PlayerPrefs.SetInt ("survival", 0);
		Application.LoadLevel (Application.loadedLevel);
		
	}
}
