﻿using UnityEngine;
using System.Collections;

public class PowerUpOneScript : MonoBehaviour {
	public Sprite power1;
	public GameObject meter;
	public GameObject container;
	public System.Collections.Generic.List<string> ballList = new System.Collections.Generic.List<string>();

	// Use this for initialization
	void Start () {
		ballList.Add ("bubble_small");
		ballList.Add ("bubble_blue");
		ballList.Add ("bubble_purple");
		ballList.Add ("bubble_red");
		ballList.Add ("bubble_yellow");

	}

	void OnMouseDown(){
		if (gameObject.GetComponent<SpriteRenderer> ().sprite == power1) {
			meter.GetComponent<MeterScript>().powerOne();
			activatePower();
		} 
	}

	void activatePower(){
		for (int i = 0; i < 10; i++) {
            //adds random ball to cannon
			int determinant = UnityEngine.Random.Range (0,5);
			container.GetComponent<ContainerScript>().add (ballList[determinant]);
		}
		int ABM = container.GetComponent<ContainerScript> ().ABMAmount;
		int TBA = container.GetComponent<ContainerScript> ().TBAmount;
		container.GetComponent<ContainerScript> ().ABMAmount = ABM + 10;
		container.GetComponent<ContainerScript> ().TBAmount = TBA + 10;
	}
}
