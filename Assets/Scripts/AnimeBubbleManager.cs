﻿using UnityEngine;
using System.Collections;

public class AnimeBubbleManager : MonoBehaviour {
	public GameObject cannon;
	public GameObject battleManager;
	public GameObject monster;
	public AudioSource popSound;
	public GameObject container;
	public GameObject containerLid;
	public GameObject meter;

	private int soundTimer;
	private bool soundPlayed;
	// Use this for initialization
	void Start () {
		popSound = GetComponent<AudioSource> ();
		resetSoundTimer ();
		soundPlayed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.name == "animeBubbleSpawn") {
			if(soundPlayed == false || soundTimer <= 0){
				popSound.Play ();
				soundPlayed = true;
				resetSoundTimer();
			}
			soundTimer -= 1;
			float step = 20* Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, containerLid.transform.position, step);
			if (transform.position == containerLid.transform.position) {
				container.GetComponent<ContainerScript>().add(gameObject.GetComponent<SpriteRenderer>().sprite.name);
				container.GetComponent<ContainerScript>().ABMAmount++;
				meter.GetComponent<MeterScript>().add();
				//battleManager.GetComponent<BattleManager>().determineDamage(gameObject);
				Destroy(gameObject);
			}
		}
		
		
	}

	void resetSoundTimer(){
		soundTimer = 10;
	}
}
