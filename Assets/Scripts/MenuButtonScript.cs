﻿using UnityEngine;
using System.Collections;

public class MenuButtonScript : MonoBehaviour {
	public Sprite menuButtonDown;
	public Sprite menuButtonUp;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = menuButtonDown;
	}
	
	void OnMouseUp(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = menuButtonUp;
		Application.LoadLevel ("Menu");
		
	}
}
