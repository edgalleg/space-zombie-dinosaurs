﻿using UnityEngine;
using System.Collections;

public class UnpauseButtonScript : MonoBehaviour {

	public Sprite continueButtonDown;
	public Sprite continueButtonUp;
	public GameObject pausePopup;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnMouseDown(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = continueButtonDown;
	}
	
	void OnMouseUp(){
		gameObject.GetComponent<SpriteRenderer> ().sprite = continueButtonUp;
		pausePopup.SetActive (false);
		
	}
}
